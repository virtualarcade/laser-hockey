package com.virtualarcade.laserhockey.entity;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.virtualarcade.laserhockey.Game;

public class Player {

	public static final int LEFT_SIDE = 1;
	public static final int RIGHT_SIDE = -1;

	public static final float DEFAULT_SIZE = 2;
	public static final float DEFAULT_SPEED = 30;
	public static final float MIN_SIZE = 0.5f;
	public static final float MAX_SIZE = 4;
	public static final float MIN_SPEED = 10;
	public static final float MAX_SPEED = 50;

	private Game game;
	private Vector2 pos, vel;
	private Circle bounds;
	private boolean actionActive;
	private int side;
	private float size = DEFAULT_SIZE;
	private float speed = DEFAULT_SPEED;

	public Player(Game game, float x, float y, int side) {
		this.game = game;
		this.side = side;
		pos = new Vector2(x, y);
		vel = new Vector2();
		bounds = new Circle(pos, size / 2f);
		actionActive = false;
	}

	public void update(float delta) {
		pos.add(vel.x * delta, vel.y * delta);
		vel.setZero();

		if(pos.x - size / 2f < game.getBounds().x) pos.x = game.getBounds().x + size / 2f;
		if(pos.x + size / 2f > game.getBounds().width) pos.x = game.getBounds().width - size / 2f;
		if(pos.y - size / 2f < game.getBounds().y) pos.y = game.getBounds().y + size / 2f;
		if(pos.y + size / 2f > game.getBounds().height) pos.y = game.getBounds().height - size / 2f;

		if(pos.y * side + size / 2f > game.getLinePos() * side) {
			pos.y = game.getLinePos() - (size / 2f) * side;
		}
	}

	public float getX() {
		return pos.x;
	}

	public float getY() {
		return pos.y;
	}

	public float getSize() {
		return size;
	}

	public float getSpeed() {
		return speed;
	}

	public Vector2 getPosition() {
		return pos;
	}

	public Vector2 getVelocity() {
		return vel;
	}

	public Circle getBounds() {
		bounds.set(pos, size / 2f);
		return bounds;
	}

	public boolean isActionActive() {
		return actionActive;
	}

	public void setSize(float size) {
		this.size = size;
		this.size = MathUtils.clamp(size, MIN_SIZE, MAX_SIZE);
	}

	public void setSpeed(float speed) {
		this.speed = speed;
		this.speed = MathUtils.clamp(speed, MIN_SPEED, MAX_SPEED);
	}

	public void setActionActive(boolean actionActive) {
		this.actionActive = actionActive;
	}

	public void translateSize(float delta) {
		size += delta;
		if(size < MIN_SIZE) size = MIN_SIZE;
		if(size > MAX_SIZE) size = MAX_SIZE;
	}

	public void translateSpeed(float delta) {
		speed += delta;
		if(speed < MIN_SPEED) speed = MIN_SPEED;
		if(speed > MAX_SPEED) speed = MAX_SPEED;
	}
}
