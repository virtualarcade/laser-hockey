package com.virtualarcade.laserhockey.entity;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.virtualarcade.laserhockey.Game;

public class Ball {

	private Game game;
	private Vector2 pos, vel;
	private float size, friction;
	private Circle bounds;

	public Ball(Game game, float x, float y) {
		this.game = game;
		pos = new Vector2(x, y);
		vel = new Vector2();
		size = 1.5f;
		friction = 0.99f;
		bounds = new Circle(pos, size / 2f);
	}

	public void reset() {
		pos.set(Game.FIELD_WIDTH / 2f, game.getLinePos());
		vel.setZero();
	}

	public float getX() {
		return pos.x;
	}

	public float getY() {
		return pos.y;
	}

	public float getSize() {
		return size;
	}

	public Vector2 getPosition() {
		return pos;
	}

	public Vector2 getVelocity() {
		return vel;
	}

	public float getFriction() {
		return friction;
	}

	public Circle getBounds() {
		bounds.set(pos, size / 2f);
		return bounds;
	}
}
