package com.virtualarcade.laserhockey.renderer;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.virtualarcade.laserhockey.Game;
import com.virtualarcade.laserhockey.entity.Player;
import com.virtualarcade.laserhockey.screen.Display;

public class PlayerRenderer {

	private Game game;
	private Player player;
	private TextureRegion paddleTexture;

	public PlayerRenderer(Game game, Player player, TextureRegion paddleTexture) {
		this.game = game;
		this.player = player;
		this.paddleTexture = paddleTexture;
	}

	public void render(Display display) {
		SpriteBatch batch = display.getBatch();
		batch.draw(paddleTexture, player.getX() - player.getSize() / 2f, player.getY() - player.getSize() / 2f, player.getSize(), player.getSize());
	}
}
