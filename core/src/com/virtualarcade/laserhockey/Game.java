package com.virtualarcade.laserhockey;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.virtualarcade.laserhockey.entity.Player;
import com.virtualarcade.laserhockey.input.InputHandler;
import com.virtualarcade.laserhockey.input.PlayerMouseListener;
import com.virtualarcade.laserhockey.screen.Display;
import com.virtualarcade.laserhockey.screen.ReadyScreen;
import com.virtualarcade.laserhockey.screen.Screen;
import com.virtualarcade.laserhockey.screen.ScreenFader;

public class Game extends ApplicationAdapter {

	public static final float FIELD_WIDTH = 9;
	public static final float FIELD_HEIGHT = 16;

	private Screen screen;
	private Display display;
	private InputHandler inputHandler;
	private ScreenFader screenFader;
	private PlayerMouseListener playerOneListener, playerTwoListener;

	private Rectangle bounds;
	private float linePos, minLinePos, maxLinePos;
	private Player playerOne, playerTwo;
	private float timeElapsed = 0;

	public int playerOneInputId = -1, playerTwoInputId = -1;

	@Override
	public void create() {
		display = new Display();
		inputHandler = new InputHandler();

		// Hide the cursor
		Gdx.graphics.setCursor(Gdx.graphics.newCursor(new Pixmap(1, 1, Format.RGBA8888), 0, 0));
		Gdx.gl.glClearColor(0, 0, 0, 1);

		// Load the assets (music, sounds, textures)
		Assets.load();

		// Start playing the background music
		Assets.pixels.play();

		// Initialize the players
		playerOne = new Player(this, FIELD_WIDTH / 2f, 5, Player.LEFT_SIDE);
		playerTwo = new Player(this, FIELD_WIDTH / 2f, FIELD_HEIGHT - 5, Player.RIGHT_SIDE);

		// Initialize the bounds
		bounds = new Rectangle(1, 1, 8, 15);
		linePos = FIELD_HEIGHT / 2f;
		minLinePos = 4;
		maxLinePos = FIELD_HEIGHT - 4;

		screenFader = new ScreenFader(this, 0.5f);
		setScreen(new ReadyScreen());
	}

	@Override
	public void render() {
		// If escape was pressed, exit the game
		if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)) Gdx.app.exit();

		// Clear the screen
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// Update the InputHandler
		inputHandler.update();

		// Update the screen
		screen.update(display.getDelta());

		// Render the screen
		screen.render(display);

		// Render the Screen Fader
		screenFader.render(display);

		timeElapsed += display.getDelta();
	}

	@Override
	public void pause() {
		screen.onPause();
	}

	@Override
	public void resume() {
		screen.onResume();
	}

	@Override
	public void dispose() {
		screen.onDestroy();
		Assets.onDestroy();
		display.onDestroy();
	}

	public void setScreen(Screen newScreen) {
		if(newScreen == null) throw new IllegalArgumentException("New screen cannot be null.");

		if(screen != null) screen.onDestroy();
		screen = newScreen;
		screen.onCreate(this);
	}

	public Display getDisplay() {
		return display;
	}

	public InputHandler getInputHandler() {
		return inputHandler;
	}

	public ScreenFader getScreenFader() {
		return screenFader;
	}

	public Player getPlayerOne() {
		return playerOne;
	}

	public Player getPlayerTwo() {
		return playerTwo;
	}

	public Rectangle getBounds() {
		return bounds;
	}

	public float getLinePos() {
		return linePos;
	}

	public float getTimeElapsed() {
		return timeElapsed;
	}

	public void moveLine(float dist) {
		linePos += dist;
		linePos = MathUtils.clamp(linePos, minLinePos, maxLinePos);
	}

	public void initPlayer(int playerNumber, int mouseId) {
		if(playerNumber == 1) {
			playerOneInputId = mouseId;
			playerOneListener = new PlayerMouseListener(mouseId, this, playerOne, -1);
			inputHandler.addMouseListener(playerOneListener);
		} else {
			playerTwoInputId = mouseId;
			playerTwoListener = new PlayerMouseListener(mouseId, this, playerTwo, 1);
			inputHandler.addMouseListener(playerTwoListener);
		}
	}

	public void reset() {
		inputHandler.clearMouseListeners();
		playerOneListener = playerTwoListener = null;
		playerOneInputId = playerTwoInputId = -1;
		Assets.onDestroy();
		create();
	}

	public enum Effect {
		LINE,
		SIZE,
		SPEED
	}
}
