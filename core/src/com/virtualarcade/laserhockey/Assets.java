package com.virtualarcade.laserhockey;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {

	private static boolean loaded = false;

	public static Music pixels, victoryIntro, victoryLoop;
	public static Sound ready, select, wom, hit, blip, bounce, laser, score, go;

	public static Sprite logo, field;
	public static Texture divider;

	private static TextureAtlas atlas;
	public static TextureRegion waitingButton, clickButton, readyButton, startButton;
	public static TextureRegion bluePaddle, orangePaddle, greyPaddle, ball;

	public static void load() {
		if(loaded) return;

		// Load music
		pixels = Gdx.audio.newMusic(Gdx.files.internal("sounds/pixels.wav"));
		victoryIntro = Gdx.audio.newMusic(Gdx.files.internal("sounds/victory-intro.wav"));
		victoryLoop = Gdx.audio.newMusic(Gdx.files.internal("sounds/victory-loop.wav"));

		// Load sounds
		ready = Gdx.audio.newSound(Gdx.files.internal("sounds/ready.ogg"));
		select = Gdx.audio.newSound(Gdx.files.internal("sounds/select.wav"));
		wom = Gdx.audio.newSound(Gdx.files.internal("sounds/wom.ogg"));
		hit = Gdx.audio.newSound(Gdx.files.internal("sounds/hit.wav"));
		blip = Gdx.audio.newSound(Gdx.files.internal("sounds/blip.ogg"));
		bounce = Gdx.audio.newSound(Gdx.files.internal("sounds/bounce.ogg"));
		laser = Gdx.audio.newSound(Gdx.files.internal("sounds/laser.wav"));
		score = Gdx.audio.newSound(Gdx.files.internal("sounds/score.wav"));
		go = Gdx.audio.newSound(Gdx.files.internal("sounds/go.ogg"));

		// Make the necessary music loop
		pixels.setLooping(true);
		victoryLoop.setLooping(true);

		// Load textures
		logo = new Sprite(new Texture("sprites/logo.png"));
		field = new Sprite(new Texture("sprites/field.png"));
		divider = new Texture("sprites/divider.png");

		atlas = new TextureAtlas(Gdx.files.internal("sprites/laser-hockey.pack"));

		waitingButton = atlas.findRegion("waiting");
		clickButton = atlas.findRegion("click");
		readyButton = atlas.findRegion("ready");
		startButton = atlas.findRegion("start");

		bluePaddle = atlas.findRegion("blue-paddle");
		orangePaddle = atlas.findRegion("orange-paddle");
		greyPaddle = atlas.findRegion("grey-paddle");
		ball = atlas.findRegion("ball");

		loaded = true;
	}

	public static void onDestroy() {
		pixels.dispose();
		ready.dispose();
		select.dispose();
		wom.dispose();
		hit.dispose();
		blip.dispose();
		bounce.dispose();
		laser.dispose();
		score.dispose();
		go.dispose();
		logo.getTexture().dispose();
		field.getTexture().dispose();
		atlas.dispose();
		loaded = false;
	}
}
