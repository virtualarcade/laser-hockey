package com.virtualarcade.laserhockey.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.virtualarcade.laserhockey.Game;

public class Display {

	public static final int SCALE = 1;
	public static final float WORLD_TO_SCREEN = Gdx.graphics.getWidth() / SCALE / Game.FIELD_WIDTH;
	public static final float SCREEN_TO_WORLD = 1f / WORLD_TO_SCREEN;

	private SpriteBatch batch;
	private ShapeRenderer shapeRenderer;
	private BitmapFont font;

	public Display() {
		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();

		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("kenvector_future.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 72;
		font = generator.generateFont(parameter);
		generator.dispose();
	}

	public SpriteBatch getBatch() {
		return batch;
	}

	public ShapeRenderer getShapeRenderer() {
		return shapeRenderer;
	}

	public BitmapFont getFont() {
		return font;
	}

	public float getWidth() {
		return Gdx.graphics.getWidth() / SCALE;
	}

	public float getHeight() {
		return Gdx.graphics.getHeight() / SCALE;
	}

	public float getDelta() {
		return Gdx.graphics.getDeltaTime();
	}

	public void onDestroy() {
		batch.dispose();
		shapeRenderer.dispose();
		font.dispose();
	}
}
