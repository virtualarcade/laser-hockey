package com.virtualarcade.laserhockey.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.virtualarcade.laserhockey.Game;

public class ScreenFader {

	private Game game;
	private float fadeDuration;

	private float alpha;
	private int dir;
	private boolean fading;
	private OrthographicCamera camera;
	private Color color;
	private Screen nextScreen;

	public ScreenFader(Game game, float fadeDuration) {
		this.game = game;
		this.fadeDuration = fadeDuration;
		alpha = 0;
		fading = false;
		color = new Color(0, 0, 0, 0);
		camera = new OrthographicCamera();
		camera.setToOrtho(false);
	}

	public void render(Display display) {
		if(fading) {
			alpha += (display.getDelta() / fadeDuration) * dir;
			alpha = MathUtils.clamp(alpha, 0, 1);

			if(dir == 1 && alpha == 1) {
				dir = -1;
				game.setScreen(nextScreen);
			}

			if(dir == -1 && alpha == 0) {
				dir = 1;
				fading = false;
			}

			color.set(0, 0, 0, alpha);

			Gdx.gl.glEnable(GL20.GL_BLEND);
			Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

			ShapeRenderer shapeRenderer = display.getShapeRenderer();
			shapeRenderer.setProjectionMatrix(camera.combined);
			shapeRenderer.setColor(color);
			shapeRenderer.begin(ShapeType.Filled);
			{
				shapeRenderer.rect(0, 0, display.getWidth(), display.getHeight());
			}
			shapeRenderer.end();

			Gdx.gl.glDisable(GL20.GL_BLEND);
		}
	}

	public void fadeToScreen(Screen screen) {
		if(!fading) {
			alpha = 0;
			fading = true;
			dir = 1;
			nextScreen = screen;
		}
	}
}
