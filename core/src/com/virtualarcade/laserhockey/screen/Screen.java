package com.virtualarcade.laserhockey.screen;

import com.virtualarcade.laserhockey.Game;

public interface Screen {

	void onCreate(Game game);
	void update(float delta);
	void render(Display display);
	void onPause();
	void onResume();
	void onDestroy();
}
