package com.virtualarcade.laserhockey.screen;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.virtualarcade.laserhockey.Assets;
import com.virtualarcade.laserhockey.Game;
import com.virtualarcade.laserhockey.entity.Player;
import com.virtualarcade.laserhockey.input.InputHandler;
import com.virtualarcade.laserhockey.input.MouseListener;

public class ReadyScreen implements Screen {

	private Game game;
	private MouseListener readyListener;
	private OrthographicCamera camera;

	private boolean playerOneReady, playerTwoReady;
	private Circle startBounds;

	@Override
	public void onCreate(Game game) {
		this.game = game;
		playerOneReady = playerTwoReady = false;

		// Create the camera and set it to the window size by default
		camera = new OrthographicCamera();
		camera.setToOrtho(false);

		startBounds = new Circle(camera.viewportWidth / 2, camera.viewportHeight / 2, Assets.startButton.getRegionWidth());

		// Initialze and add the ready mouse listener to the input handler
		readyListener = new ReadyListener();
		game.getInputHandler().addMouseListener(readyListener);

		// Flip the waiting button to face player two
		Assets.waitingButton.flip(true, true);

		// Rotate the logo 90 degrees
		Assets.logo.rotate90(false);
		// Flip the width and height after rotating 90 degrees
		Assets.logo.setSize(Assets.logo.getHeight(), Assets.logo.getWidth());

		Assets.pixels.setVolume(0.5f);
	}

	@Override
	public void update(float delta) {
		// Update the players
		game.getPlayerOne().update(delta);
		game.getPlayerTwo().update(delta);

		if(playerOneReady && playerTwoReady) {
			Player p1 = game.getPlayerOne();
			Player p2 = game.getPlayerTwo();

			if((p1.isActionActive() && startBounds.contains(p1.getPosition().cpy().scl(Display.WORLD_TO_SCREEN)))
			|| (p2.isActionActive() && startBounds.contains(p2.getPosition().cpy().scl(Display.WORLD_TO_SCREEN)))) {
				Assets.wom.play();
				game.getScreenFader().fadeToScreen(new GameScreen());
			}
		}
	}

	@Override
	public void render(Display display) {
		SpriteBatch batch = display.getBatch();

		// All buttons and paddles are the same size graphically, so store the width, height, centerX, and centerY once
		float width = Assets.clickButton.getRegionWidth();
		float height = Assets.clickButton.getRegionHeight();
		float centerX = camera.viewportWidth / 2 - width / 2;
		float centerY = camera.viewportHeight / 2 - height / 2;

		Player playerOne = game.getPlayerOne(), playerTwo = game.getPlayerTwo();
		float playerOneX = (playerOne.getX() - playerOne.getSize() / 2f) * Display.WORLD_TO_SCREEN;
		float playerOneY = (playerOne.getY() - playerOne.getSize() / 2f) * Display.WORLD_TO_SCREEN;
		float playerTwoX = (playerTwo.getX() - playerTwo.getSize() / 2f) * Display.WORLD_TO_SCREEN;
		float playerTwoY = (playerTwo.getY() - playerTwo.getSize() / 2f) * Display.WORLD_TO_SCREEN;

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		{
			// Render player one's ready button and paddle
			TextureRegion playerOneButton = Assets.clickButton;
			TextureRegion playerOnePaddle = Assets.greyPaddle;
			if(playerOneReady) {
				playerOneButton = Assets.readyButton;
				playerOnePaddle = Assets.bluePaddle;
			}
			batch.draw(playerOneButton, centerX, 150);
			batch.draw(playerOnePaddle, playerOneX, playerOneY, playerOne.getSize() * Display.WORLD_TO_SCREEN, playerOne.getSize() * Display.WORLD_TO_SCREEN);

			// Render player two's ready button and paddle
			TextureRegion playerTwoButton = Assets.waitingButton;
			TextureRegion playerTwoPaddle = Assets.greyPaddle;
			float buttonSize = playerTwoButton.getRegionWidth();
			float offset = 0;
			if(playerOneReady) {
				if(playerTwoReady) {
					playerTwoButton = Assets.readyButton;
					playerTwoPaddle = Assets.orangePaddle;
					offset = buttonSize;
					buttonSize *= -1;
				} else {
					playerTwoButton = Assets.clickButton;
				}
			}
			batch.draw(playerTwoButton, centerX + offset, camera.viewportHeight - height - 150 + offset, buttonSize, buttonSize);
			batch.draw(playerTwoPaddle, playerTwoX, playerTwoY, playerTwo.getSize() * Display.WORLD_TO_SCREEN, playerTwo.getSize() * Display.WORLD_TO_SCREEN);

			// If both players are ready, show the start button
			if(playerOneReady && playerTwoReady) {
				batch.draw(Assets.startButton, centerX, centerY);
			}

			// Render the logo
			Assets.logo.setCenter(Assets.logo.getHeight() / 2, Assets.logo.getWidth() / 2);
			Assets.logo.setPosition(100, camera.viewportHeight / 2 - Assets.logo.getHeight() / 2);
			Assets.logo.draw(batch);
		}
		batch.end();
	}

	@Override
	public void onPause() {

	}

	@Override
	public void onResume() {

	}

	@Override
	public void onDestroy() {
		game.getInputHandler().removeMouseListener(readyListener);
	}

	class ReadyListener implements MouseListener {

		@Override
		public void mouseDown(MouseButton button, int id) {
			// If the playerOne id is not set
			if(!playerOneReady) {
				// Initialze the mouse listener for player one
				game.initPlayer(1, id);

				// Play the ready sound effect
				Assets.ready.play(1);

				// Player one is now ready
				playerOneReady = true;

				// Flip the click button to face player two
				Assets.clickButton.flip(true, true);
			}
			// Else, if player one already maps to a mouse and player two does not
			else if(!playerTwoReady) {
				// If the id is not the same as player one (different mouse), map player two's id to the mouse clicked
				if(id != game.playerOneInputId) {
					// Initialze the mouse listener for player one
					game.initPlayer(2, id);

					// Play the ready sound effect
					Assets.ready.play(1);

					// Player two is now ready
					playerTwoReady = true;
				}
			}
		}

		@Override
		public void mouseUp(MouseButton button, int id) {}

		@Override
		public void mouseMoved(int dx, int dy, int id) {}

		@Override
		public int getID() {
			return InputHandler.ANY;
		}
	}
}
