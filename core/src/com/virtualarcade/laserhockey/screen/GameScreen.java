package com.virtualarcade.laserhockey.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Music.OnCompletionListener;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.utils.Align;
import com.virtualarcade.laserhockey.Assets;
import com.virtualarcade.laserhockey.Game;
import com.virtualarcade.laserhockey.Game.Effect;
import com.virtualarcade.laserhockey.entity.Ball;
import com.virtualarcade.laserhockey.renderer.PlayerRenderer;

public class GameScreen implements Screen {

	public final int POINTS_TO_WIN = 10;

	private Game game;
	private OrthographicCamera uiCamera, camera;
	private PlayerRenderer playerOneRenderer, playerTwoRenderer;

	private Ball ball;
	private Rectangle[] fieldBorder;
	private int playerOneScore, playerTwoScore;
	private int playerScored, playerWon;
	private Effect positiveEffect, negativeEffect;
	private float scoredStateDuration = 2;
	private float timeElapsed = 0;

	@Override
	public void onCreate(Game game) {
		this.game = game;

		ball = new Ball(game, Game.FIELD_WIDTH / 2f, Game.FIELD_HEIGHT / 2f);
		playerOneScore = playerTwoScore = playerScored = playerWon = 0;

		fieldBorder = new Rectangle[6];
		fieldBorder[0] = new Rectangle(0, 0, 3, 1); // Bottom left
		fieldBorder[1] = new Rectangle(6, 0, 3, 1); // Bottom right
		fieldBorder[2] = new Rectangle(0, 1, 1, Game.FIELD_HEIGHT - 2); // Left
		fieldBorder[3] = new Rectangle(Game.FIELD_WIDTH - 1, 1, 1, Game.FIELD_HEIGHT - 2); // Right
		fieldBorder[4] = new Rectangle(0, Game.FIELD_HEIGHT - 1, 3, 1); // Top left
		fieldBorder[5] = new Rectangle(6, Game.FIELD_HEIGHT - 1, 3, 1); // Top right

		uiCamera = new OrthographicCamera();
		uiCamera.setToOrtho(false);

		camera = new OrthographicCamera();
		camera.setToOrtho(false, Game.FIELD_WIDTH, Game.FIELD_HEIGHT);
		playerOneRenderer = new PlayerRenderer(game, game.getPlayerOne(), Assets.bluePaddle);
		playerTwoRenderer = new PlayerRenderer(game, game.getPlayerTwo(), Assets.orangePaddle);
		Assets.field.setBounds(0, 0, camera.viewportWidth, camera.viewportHeight);
		Assets.field.rotate90(false);

		Assets.pixels.setVolume(1);
	}

	@Override
	public void update(float delta) {
		if(playerScored != 0) {
			if(positiveEffect == Effect.LINE) {
				float dist = delta / scoredStateDuration * (playerScored == 1 ? 1 : -1);
				game.moveLine(dist);
			}
			else if(positiveEffect == Effect.SIZE) {
				float dist = delta / scoredStateDuration;
				dist *= 0.5f;
				if(playerScored == 1) game.getPlayerOne().setSize(game.getPlayerOne().getSize() + dist);
				else game.getPlayerTwo().setSize(game.getPlayerTwo().getSize() + dist);
			}

			if(negativeEffect == Effect.SIZE) {
				float dist = delta / scoredStateDuration;
				dist *= 0.5f;
				if(playerScored == 2) game.getPlayerOne().setSize(game.getPlayerOne().getSize() - dist);
				else game.getPlayerTwo().setSize(game.getPlayerTwo().getSize() - dist);
			}

			timeElapsed += delta;
			if(timeElapsed >= scoredStateDuration) {
				playerScored = 0;
				timeElapsed = 0;
				ball.reset();
				Assets.go.play();
			}

			return;
		}

		if(playerWon != 0) {
			if(Gdx.input.justTouched()) {
				onDestroy();
				game.reset();
			}

			return;
		}

		Vector2 pos = ball.getPosition();
		ball.getVelocity().scl(ball.getFriction());
		pos.x = checkCollisions(pos.x + (ball.getVelocity().x * delta), pos.y, Axis.X);
		pos.y = checkCollisions(pos.x, pos.y + (ball.getVelocity().y * delta), Axis.Y);

		// Prevent the ball from leaving the field
		Vector2 vel = ball.getVelocity();
		if(vel.x < 0 && pos.x + ball.getSize() / 2f <= 0) pos.x = ball.getSize() / 2f + 1;
		if(vel.x > 0 && pos.x - ball.getSize() / 2f > Game.FIELD_WIDTH) pos.x = Game.FIELD_WIDTH - ball.getSize() / 2f - 1;
		if(vel.y < 0 && pos.y + ball.getSize() / 2f <= 0 && (pos.x <= 3 - ball.getSize() / 2f || pos.x >= 6 + ball.getSize() / 2f)) pos.y = ball.getSize() / 2f + 1;
		if(vel.y > 0 && pos.y - ball.getSize() / 2f >= Game.FIELD_HEIGHT && (pos.x <= 3 - ball.getSize() / 2f || pos.x >= 6 + ball.getSize() / 2f)) pos.y = Game.FIELD_HEIGHT - ball.getSize() / 2f - 1;

		Circle p1Bounds = game.getPlayerOne().getBounds();
		Circle p2Bounds = game.getPlayerTwo().getBounds();
		if(ball.getBounds().overlaps(p1Bounds)) {
			Vector2 ballVel = new Vector2(ball.getX() - p1Bounds.x, ball.getY() - p1Bounds.y);
			ballVel.nor();
			float ballX = p1Bounds.x + ballVel.x * (ball.getSize() / 2f + p1Bounds.radius);
			float ballY = p1Bounds.y + ballVel.y * (ball.getSize() / 2f + p1Bounds.radius);

			if(game.getPlayerOne().getVelocity().len() > 0) {
				ballVel.scl(game.getPlayerOne().getVelocity().len());
			} else {
				ballVel.scl(ball.getVelocity().len());
			}

			ball.getPosition().set(ballX, ballY);
			ball.getVelocity().set(ballVel);
			Assets.bounce.play();
		}

		if(ball.getBounds().overlaps(p2Bounds)) {
			Vector2 ballVel = new Vector2(ball.getX() - p2Bounds.x, ball.getY() - p2Bounds.y);
			ballVel.nor();
			float ballX = p2Bounds.x + ballVel.x * (ball.getSize() / 2f + p2Bounds.radius);
			float ballY = p2Bounds.y + ballVel.y * (ball.getSize() / 2f + p2Bounds.radius);

			if(game.getPlayerTwo().getVelocity().len() > 0) {
				ballVel.scl(game.getPlayerTwo().getVelocity().len());
			} else {
				ballVel.scl(ball.getVelocity().len());
			}

			ball.getPosition().set(ballX, ballY);
			ball.getVelocity().set(ballVel);
			Assets.bounce.play();
		}

		if(Math.abs(ball.getVelocity().x) < 0.001f) ball.getVelocity().x = 0;
		if(Math.abs(ball.getVelocity().y) < 0.001f) ball.getVelocity().y = 0;

		// Update the players
		game.getPlayerOne().update(delta);
		game.getPlayerTwo().update(delta);

		checkForScore();
	}

	@Override
	public void render(Display display) {
		SpriteBatch batch = display.getBatch();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		{
			// Render the field and divider
			Assets.field.draw(batch);
			float dx = camera.viewportWidth / 2f - (Assets.divider.getWidth() / 2f) * Display.SCREEN_TO_WORLD;
			float dy = game.getLinePos() - (Assets.divider.getHeight() / 2f) * Display.SCREEN_TO_WORLD;
			batch.draw(Assets.divider, dx, dy, Assets.divider.getWidth() * Display.SCREEN_TO_WORLD, Assets.divider.getHeight() * Display.SCREEN_TO_WORLD);

			// Render the players
			playerOneRenderer.render(display);
			playerTwoRenderer.render(display);

			// Render the ball
			batch.draw(Assets.ball, ball.getX() - ball.getSize() / 2f, ball.getY() - ball.getSize() / 2f, ball.getSize(), ball.getSize());
		}
		batch.end();

		batch.setProjectionMatrix(uiCamera.combined);
		batch.begin();
		{
			// Render the scores
			display.getFont().draw(batch, "" + playerOneScore, 0, 120, uiCamera.viewportWidth, Align.center, true);
			display.getFont().getData().setScale(-1, -1);
			display.getFont().draw(batch, "" + playerTwoScore, 0, uiCamera.viewportHeight - 180 - display.getFont().getLineHeight(), uiCamera.viewportWidth, Align.center, true);
			display.getFont().getData().setScale(1, 1);

			if(playerScored != 0) {
				if(positiveEffect == Effect.SPEED) {
					if(playerScored == 1) {
						display.getFont().draw(batch, "SPEED INCREASED", 0, 300, uiCamera.viewportWidth, Align.center, true);
					} else {
						display.getFont().getData().setScale(-1, -1);
						display.getFont().draw(batch, "SPEED INCREASED", 0, uiCamera.viewportHeight - 350 - display.getFont().getLineHeight(), uiCamera.viewportWidth, Align.center, true);
						display.getFont().getData().setScale(1, 1);
					}
				}

				if(negativeEffect == Effect.SPEED) {
					if(playerScored == 2) {
						display.getFont().draw(batch, "SPEED DECREASED", 0, 300, uiCamera.viewportWidth, Align.center, true);
					} else {
						display.getFont().getData().setScale(-1, -1);
						display.getFont().draw(batch, "SPEED DECREASED", 0, uiCamera.viewportHeight - 350 - display.getFont().getLineHeight(), uiCamera.viewportWidth, Align.center, true);
						display.getFont().getData().setScale(1, 1);
					}
				}
			}

			if(playerWon != 0) {
				String playerOneText, playerTwoText;

				if(playerWon == 1) {
					playerOneText = "YOU WIN!";
					playerTwoText = "YOU LOSE!";
				} else {
					playerOneText = "YOU LOSE!";
					playerTwoText = "YOU WIN!";
				}

				display.getFont().draw(batch, playerOneText, 0, 300 + MathUtils.sin(game.getTimeElapsed() * 5) * 10, uiCamera.viewportWidth, Align.center, true);
				display.getFont().draw(batch, "CLICK TO RESET", 0, 600, uiCamera.viewportWidth, Align.center, true);
				display.getFont().getData().setScale(-1, -1);
				display.getFont().draw(batch, playerTwoText, 0, uiCamera.viewportHeight - 350 - display.getFont().getLineHeight() + MathUtils.sin(game.getTimeElapsed() * 5) * 10, uiCamera.viewportWidth, Align.center, true);
				display.getFont().draw(batch, "CLICK TO RESET", 0, uiCamera.viewportHeight - 650 - display.getFont().getLineHeight(), uiCamera.viewportWidth, Align.center, true);
				display.getFont().getData().setScale(1, 1);
			}
		}
		batch.end();
	}

	private float checkCollisions(float x, float y, int axis) {
		Circle c = ball.getBounds();

		if(axis == Axis.X) {
			c.x = x;
			for(Rectangle r : fieldBorder) {
				if(Intersector.overlaps(c, r)) {
					// Intersecting from right
					if(Math.abs(c.x - (r.x + r.width)) <= c.radius && ball.getVelocity().x < 0) {
						c.x = r.x + r.width + c.radius;
						ball.getVelocity().x *= -1;
					}
					// Intersecting from left
					else if(Math.abs(r.x - c.x) <= c.radius && ball.getVelocity().x > 0) {
						c.x = r.x - c.radius;
						ball.getVelocity().x *= -1;
					}

					Assets.hit.play();
				}
			}

			return c.x;
		}

		if(axis == Axis.Y) {
			c.y = y;
			for(Rectangle r : fieldBorder) {
				if(Intersector.overlaps(c, r)) {
					// Intersecting on top
					if(Math.abs(c.y - (r.y + r.height)) <= c.radius && ball.getVelocity().y < 0) {
						c.y = r.y + r.height + c.radius;
						ball.getVelocity().y *= -1;
					}
					// Intersecting from bottom
					else if(Math.abs(r.y - c.y) <= c.radius && ball.getVelocity().y > 0) {
						c.y = r.y - c.radius;
						ball.getVelocity().y *= -1;
					}

					Assets.hit.play();
				}
			}

			return c.y;
		}

		return axis == 0 ? c.x : c.y;
	}

	private void checkForScore() {
		if(ball.getY() + ball.getSize() / 2f <= 0) {
			playerTwoScore++;
			playerScored = 2;
		}

		if(ball.getY() - ball.getSize() / 2f >= Game.FIELD_HEIGHT) {
			playerOneScore++;
			playerScored = 1;
		}

		if(ball.getVelocity().x == 0 && ball.getVelocity().y == 0) {
			if(ball.getY() + ball.getSize() / 2f <= 1) {
				playerTwoScore++;
				playerScored = 2;
			}

			if(ball.getY() - ball.getSize() / 2f >= Game.FIELD_HEIGHT - 1) {
				playerOneScore++;
				playerScored = 1;
			}
		}

		if(playerScored != 0) {
			if(MathUtils.random(2) == 0) {
				positiveEffect = negativeEffect = Effect.LINE;
			} else {
				positiveEffect = MathUtils.randomBoolean() ? Effect.SIZE : Effect.SPEED;
				negativeEffect = MathUtils.randomBoolean() ? Effect.SIZE : Effect.SPEED;

				if(positiveEffect == Effect.SPEED) {
					if(playerScored == 1) game.getPlayerOne().setSpeed(game.getPlayerOne().getSpeed() + 7.5f);
					if(playerScored == 2) game.getPlayerTwo().setSpeed(game.getPlayerTwo().getSpeed() + 7.5f);
				}

				if(negativeEffect == Effect.SPEED) {
					if(playerScored == 2) game.getPlayerOne().setSpeed(game.getPlayerOne().getSpeed() - 7.5f);
					if(playerScored == 1) game.getPlayerTwo().setSpeed(game.getPlayerTwo().getSpeed() - 7.5f);
				}
			}
		}

		if(playerOneScore >= POINTS_TO_WIN) playerWon = 1;
		if(playerTwoScore >= POINTS_TO_WIN) playerWon = 2;

		// Restricts playing the score sound effect if a player won (cue the victory music instead)
		if(playerScored != 0 && playerWon == 0) {
			Assets.score.play();
		}

		if(playerWon != 0) {
			playerScored = 0;
			Assets.pixels.stop();
			Assets.victoryIntro.setOnCompletionListener(new VictoryMusicCompletionListener());
			Assets.victoryIntro.play();
		}
	}

	class VictoryMusicCompletionListener implements OnCompletionListener {
		@Override
		public void onCompletion(Music music) {
			Assets.victoryLoop.play();
		}
	}

	@Override
	public void onPause() {

	}

	@Override
	public void onResume() {

	}

	@Override
	public void onDestroy() {
		Assets.victoryIntro.setOnCompletionListener(null);
		Assets.victoryIntro.stop();
		Assets.victoryLoop.stop();
	}

	public class Axis {
		public static final int X = 0;
		public static final int Y = 1;
	}
}
