package com.virtualarcade.laserhockey.input;

import com.badlogic.gdx.utils.Array;
import com.virtualarcade.laserhockey.input.MouseListener.MouseButton;
import net.java.games.input.*;
import net.java.games.input.Component.Identifier;
import net.java.games.input.Controller.Type;

import java.util.HashMap;
import java.util.Map;

public class InputHandler {

	public static final int ANY = -1;

	private Map<Integer, Mouse> mice;
	private Array<MouseListener> mouseListeners;
	private Event event;

	public InputHandler() {
		mice = new HashMap<Integer, Mouse>();
		mouseListeners = new Array<MouseListener>();
		event = new Event();

		Controller[] controllers = ControllerEnvironment.getDefaultEnvironment().getControllers();
		for(int i = 0; i < controllers.length; i++) {
			Controller c = controllers[i];
			if(c.getType() == Type.MOUSE) {
				mice.put(i, (Mouse) c);
			}
		}
	}

	public void addMouseListener(MouseListener listener) {
		if(!mouseListeners.contains(listener, true)) mouseListeners.add(listener);
	}

	public void removeMouseListener(MouseListener listener) {
		mouseListeners.removeValue(listener, true);
	}

	public void clearMouseListeners() {
		mouseListeners.clear();
	}

	public void update() {
		for(int id : mice.keySet()) {
			Mouse mouse = mice.get(id);

			if(!mouse.poll()) return;
			EventQueue eventQueue = mouse.getEventQueue();

			boolean mouseMoved = false;
			int dx = 0, dy = 0;

			while(eventQueue.getNextEvent(event)) {
				Component component = event.getComponent();
				Identifier identifier = component.getIdentifier();

				if(identifier == mouse.getX().getIdentifier()) {
					mouseMoved = true;
					dx = (int) event.getValue();
				}

				if(identifier == mouse.getY().getIdentifier()) {
					mouseMoved = true;
					dy = (int) event.getValue();
				}

				if(identifier == mouse.getLeft().getIdentifier()) {
					sendButtonEvent(id, MouseButton.LEFT, event.getValue() == 1);
				}

				if(identifier == mouse.getRight().getIdentifier()) {
					sendButtonEvent(id, MouseButton.RIGHT, event.getValue() == 1);
				}

				if(mouse.getMiddle() != null && identifier == mouse.getMiddle().getIdentifier()) {
					sendButtonEvent(id, MouseButton.MIDDLE, event.getValue() == 1);
				}
			}

			if(mouseMoved) sendMoveEvent(id, dx, dy);
		}
	}

	private void sendButtonEvent(int mouseId, MouseButton button, boolean pressed) {
		for(MouseListener l : mouseListeners) {
			if(l.getID() == ANY || l.getID() == mouseId) {
				if(pressed) l.mouseDown(button, mouseId);
				else l.mouseUp(button, mouseId);
			}
		}
	}

	private void sendMoveEvent(int mouseId, int dx, int dy) {
		for(MouseListener l : mouseListeners) {
			if(l.getID() == ANY || l.getID() == mouseId) {
				l.mouseMoved(dx, dy, mouseId);
			}
		}
	}
}
