package com.virtualarcade.laserhockey.input;

import com.virtualarcade.laserhockey.Game;
import com.virtualarcade.laserhockey.entity.Player;
import com.virtualarcade.laserhockey.screen.Display;

public class PlayerMouseListener implements MouseListener {

	private int id;
	private Game game;
	private Player player;
	private int playerNum;

	public PlayerMouseListener(int id, Game game, Player player, int playerNum) {
		this.id = id;
		this.game = game;
		this.player = player;
		this.playerNum = playerNum;
	}

	@Override
	public void mouseMoved(int dx, int dy, int id) {
		float dXWorld = dx * -playerNum * Display.SCREEN_TO_WORLD;
		float dYWorld = dy * playerNum * Display.SCREEN_TO_WORLD;

		player.getVelocity().set(dXWorld, dYWorld).scl(player.getSpeed());
	}

	@Override
	public void mouseDown(MouseButton button, int id) {
		if(button == MouseButton.LEFT) player.setActionActive(true);
	}

	@Override
	public void mouseUp(MouseButton button, int id) {
		if(button == MouseButton.LEFT) player.setActionActive(false);
	}

	@Override
	public int getID() {
		return id;
	}
}
