package com.virtualarcade.laserhockey.input;

public interface MouseListener {

	void mouseMoved(int dx, int dy, int id);
	void mouseDown(MouseButton button, int id);
	void mouseUp(MouseButton button, int id);

	int getID();

	enum MouseButton {
		LEFT,
		RIGHT,
		MIDDLE
	}
}
