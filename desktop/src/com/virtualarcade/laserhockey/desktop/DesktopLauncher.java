package com.virtualarcade.laserhockey.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.virtualarcade.laserhockey.Game;

public class DesktopLauncher {

	public static void main(String[] arg) {
		/*Path directory;
		try {
			directory = Paths.get(Game.class.getProtectionDomain().getCodeSource().getLocation().toURI()).toAbsolutePath().resolveSibling("");
			System.out.println("Path: " + directory.toString());
			System.setProperty("java.library.path", directory.toString() + "//natives");
			Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
			fieldSysPath.setAccessible(true);
			fieldSysPath.set(null, null);
		} catch(Exception e) {
			e.printStackTrace();
		}*/

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Laser Hockey";
		config.width = 1920;
		config.height = 1080;
		config.fullscreen = true;

		new LwjglApplication(new Game(), config);
	}
}
